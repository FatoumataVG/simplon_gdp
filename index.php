<?php require_once './includes/process.php'; ?>
<div class="container">
    <?php include './includes/header.php';?>
    <a class="add_project" href="./includes/add_project.php">Ajouter un projet</a>
    <div class="projects_list">

            <table>
                <thead>
                    <tr>
                        <th>Projet</th>
                        <th> </th>

                    </tr>
                </thead>
                <?php
                while ($row = $project_list->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['name_project']; ?></td>

                    <td class="double">
                        <a class="btn-modif" href="./includes/project.php?afficher=<?php echo $row['Id']; ?>">Voir |</a>
                        <a class="btn-modif" href="./includes/add_project.php?modifier=<?php echo $row['Id']; ?>">Modifier |</a>
                        <a class="btn-supp" href="index.php?supprimer=<?php echo $row['Id']; ?>">Supprimer</a>
                    </td>
                
                </tr>
                <?php endwhile; ?>
            </table>

        </div>
        <a class="add_dev" href="./includes/add_dev.php">Ajouter un dev</a>

</div>
<?php include './includes/footer.php';?>

