-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 17, 2021 at 04:47 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Simplon_GDP`
--

-- --------------------------------------------------------

--
-- Table structure for table `Assignations`
--

CREATE TABLE `Assignations` (
  `Id_project` int NOT NULL,
  `Id_dev` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Clients`
--

CREATE TABLE `Clients` (
  `Id` int NOT NULL,
  `name_client` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Devs`
--

CREATE TABLE `Devs` (
  `Id` int NOT NULL,
  `lastname_dev` varchar(255) NOT NULL,
  `firstname_dev` varchar(255) NOT NULL,
  `Id_level` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Devs`
--

INSERT INTO `Devs` (`Id`, `lastname_dev`, `firstname_dev`, `Id_level`) VALUES
(9, 'Bak', 'Hava', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Levels`
--

CREATE TABLE `Levels` (
  `Id` int NOT NULL,
  `level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Levels`
--

INSERT INTO `Levels` (`Id`, `level`) VALUES
(1, 'Junior'),
(2, 'Sénior'),
(3, 'Tech Lead');

-- --------------------------------------------------------

--
-- Table structure for table `Projects`
--

CREATE TABLE `Projects` (
  `Id` int NOT NULL,
  `name_project` varchar(255) NOT NULL,
  `description_project` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Projects`
--

INSERT INTO `Projects` (`Id`, `name_project`, `description_project`) VALUES
(1, 'Refonte du Site Web M2K', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi deserunt beatae, aperiam ullam officia provident dolorem? Quasi esse quos itaque natus quaerat id, voluptates quas suscipit non, a quia consequuntur explicabo ipsam ad voluptatem. Eos hic deleniti natus repellat totam.'),
(2, 'Site Web de l\'association Coréenne', 'Nulla elementum tincidunt risus elementum laoreet. Ut sagittis non risus in commodo. Maecenas sapien quam, viverra nec purus dapibus, consequat imperdiet diam. Nunc elementum felis nec diam sodales, sit amet lobortis ante luctus. Quisque magna nulla, consectetur ac dolor eu, blandit rhoncus turpis. Donec orci ipsum, facilisis nec lacinia in, interdum nec magna. Mauris pharetra risus quis nisl mattis scelerisque. Mauris iaculis ornare mattis. Suspendisse nisi justo, rutrum non velit non, vestibulum pharetra dui. Morbi eros est, tincidunt nec aliquam eget, suscipit in sem. Aliquam a nisl massa. Proin tellus lorem, tincidunt sit amet feugiat eu, imperdiet sit amet dui. Quisque cursus ligula iaculis nunc placerat euismod. Nunc in erat sit amet ante tincidunt pharetra eget vulputate tellus. Aliquam consectetur eget orci sed eleifend. Curabitur ut ante convallis, pretium justo sed, pharetra enim. '),
(3, 'Application Mobile Tisséo', 'Pellentesque sed odio in lorem sodales feugiat. Nunc ullamcorper convallis nunc, non egestas magna dictum sed. Maecenas et dui urna. In posuere dolor a lacus aliquam semper. Etiam massa nibh, volutpat eget commodo at, tristique luctus quam. Quisque tempus enim sit amet lacus pharetra tincidunt. In a pharetra urna. Pellentesque aliquet turpis nec dui porta, venenatis fermentum erat rutrum. In at sagittis diam. Aenean vel facilisis purus. Donec dapibus id risus at rutrum. Phasellus porttitor sit amet diam ut imperdiet. Vivamus lacinia diam molestie nulla auctor eleifend. Pellentesque mollis nunc id bibendum auctor. Praesent vel sem non justo tempor euismod. Duis venenatis erat non lectus pulvinar imperdiet. '),
(4, 'Application Mobile k-drama', 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam a nibh sagittis, suscipit libero et, sagittis lectus. Maecenas in eleifend lacus. Pellentesque fermentum urna turpis, eget luctus enim pretium sed. Cras dui nibh, faucibus non fringilla et, sagittis a eros. Nulla in augue sed felis efficitur eleifend. Morbi placerat sem eu ex aliquam rhoncus. Pellentesque maximus lorem molestie laoreet ullamcorper. Proin laoreet interdum ligula, vel lobortis est. Vestibulum vel risus a tortor varius malesuada. Nunc sapien mi, rhoncus vel lacinia in, tincidunt nec tellus. Donec est massa, molestie cursus leo at, consectetur feugiat dui. Donec ultrices ut lectus et maximus. Cras porttitor lectus quis ornare convallis. Sed eget dui at dolor porta gravida. Phasellus ornare ante arcu, in efficitur magna ultricies quis.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Assignations`
--
ALTER TABLE `Assignations`
  ADD PRIMARY KEY (`Id_project`,`Id_dev`);

--
-- Indexes for table `Clients`
--
ALTER TABLE `Clients`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Devs`
--
ALTER TABLE `Devs`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Levels`
--
ALTER TABLE `Levels`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Projects`
--
ALTER TABLE `Projects`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Clients`
--
ALTER TABLE `Clients`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Devs`
--
ALTER TABLE `Devs`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `Levels`
--
ALTER TABLE `Levels`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Projects`
--
ALTER TABLE `Projects`
  MODIFY `Id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
