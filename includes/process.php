<?php

    require_once 'connexion.php';
    $update = false;
    $update_dev == false;

//connexion à la base de données
    $con = mysqli_connect($hn, $un, $pw, $db);

    if(mysqli_connect_errno()){
        echo "Failed to connect !";
        exit();
    }

//afficher les projets
    $project_list = $con->query("SELECT * FROM Projects")
    or die ($con->error);



//afficher un projet
if (isset($_GET['afficher'])){
    $id_project = (int) $_GET['afficher'];
    $result_project = $con->query("SELECT * FROM Projects WHERE id=$id_project") or die($con->error);
    if ($result_project){
        $row = $result_project->fetch_array();
        $project = $row['name_project'];
    }
    
}

//ajouter un projet
if(isset($_POST['enregistrer'])=== true){
    $name_project = $_POST['post_name_project'];
    $description_project = $_POST['post_description_project'];

    $con->query("INSERT INTO Projects(name_project, description_project) VALUES ('$name_project', '$description_project')")
    or die($con->error);


    header("location: /index.php");
}

//selectionner pour modifier un projet
if (isset($_GET['modifier'])){
    $id_project = (int) $_GET['modifier'];
    $update = true;
    $result_project = $con->query("SELECT * FROM Projects WHERE Id=$id_project") or die($con->error);
    if ($result_project){
        $row = $result_project->fetch_array();
        $project = $row['name_project'];
        $description_project = $row['description_project'];
    }
    
}

//modifier un projet
if (isset($_POST['update'])){
    $id_modif_projet = $_POST['id_projet'];
    $modif_project = $_POST['post_name_project'];
    $modif_description_project = $_POST['post_description_project'];
    $sql = "UPDATE Projects SET name_project='$modif_project', description_project='$modif_description_project' WHERE Id=$id_modif_projet";

    $con->query($sql) or die($con->error);

    header("location: /index.php");
}


//supprimer un projet
if (isset($_GET['supprimer'])){
    $id = (int) $_GET['supprimer'];
    $con->query("DELETE FROM Projects WHERE Id=$id") or die($con->error);

    header("location: index.php");
}

//ajouter un dev
if(isset($_POST['enregistrer_dev'])=== true){
    $last_name = $_POST['post_lastname_dev'];
    $first_name = $_POST['post_firstname_dev'];
    $level = $_POST['post_dev_level'];

    $con->query("INSERT INTO Devs (lastname_dev, firstname_dev, Id_level) VALUES ('$last_name', '$first_name', $level)")
    or die($con->error);

    header("location: /index.php");
}

//afficher les devs
$dev_list = $con->query("SELECT * FROM Devs")
or die ($con->error);

// Liste des niveaux
$query_niveaux = $con->query("SELECT * FROM Levels")
or die ($con->error);

?>